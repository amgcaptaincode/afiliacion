package com.afiliacion.mx.constants

object AppConstants {

    const val CAMERA_PERMISSION_CODE = 444;
    const val STORAGE_PERMISSION_CODE = 333;

}