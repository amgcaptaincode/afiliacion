package com.afiliacion.mx.data.repository

import com.afiliacion.mx.constants.FirebaseConstants
import com.afiliacion.mx.model.entities.DataIne
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore

class AfiliacionRepository {

    val firestore = FirebaseFirestore.getInstance()

    fun saveDataIne(dataIne: DataIne) : Task<Void> {

        return firestore.collection(FirebaseConstants.DATA)
            .document(dataIne.claveElector!!)
            .set(dataIne)

    }

}