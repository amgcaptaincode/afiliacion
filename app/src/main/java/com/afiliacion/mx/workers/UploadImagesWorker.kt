package com.afiliacion.mx.workers

import android.content.Context
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.afiliacion.mx.utils.FirebaseStorageUtils

class UploadImagesWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {

    override fun doWork(): Result {
        uploadImages(inputData)

        return Result.success()
    }

    private fun uploadImages(inputData: Data) {

        val fileName = inputData.getString("fileName")!!
        val folder = inputData.getString("folder")!!
        val byteArray = inputData.getByteArray("byteArray")!!

        // fileName: String, folder: String, bitmap: Bitmap

        /*FirebaseStorageUtils.uploadFile(
            fileName,
            folder,
            byteArray
        )*/

    }
}