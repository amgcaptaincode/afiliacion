package com.afiliacion.mx.model.entities

import android.os.Parcelable
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataIne(
    var nombre: String? = null,
    var fechaNacimiento: Long? = null,
    var sexo: String? = null,
    var domicilio: String? = null,
    var claveElector: String? = null,
    var curp: String? = null,
    var anioRegistro: Int? = null,
    var estado: Int? = null,
    var municipio: Int? = null,
    var seccion: Int? = null,
    var localidad: Int? = null,
    var emision: Int? = null,
    var vigencia: Int? = null,
    var fotoFrontal: String? = null,
    var fotoReverso: String? = null,
    var fotoFirma: String? = null,
    var status: Boolean? = null,
    var numeroDocumento: String? = null,
    var creador: String? = null

) : Parcelable {

    override fun toString(): String {
        return Gson().toJson(this)
    }

}