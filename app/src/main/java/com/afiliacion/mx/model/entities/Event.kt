package com.afiliacion.mx.model.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Event(
    val name: String?,
    val description: String?,
    val location: String?,
    val dateTime: Long?
) : Parcelable {
}