package com.afiliacion.mx.utils

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.view.inputmethod.InputMethodManager
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import com.microblink.MicroblinkSDK
import com.microblink.util.Log
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

object Toolbox {

    fun closeKeyBoard(activity: Activity) {
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun convertDateToTimestamp(dateString: String): Long {
        var time = 0L
        try {
            val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            val d = sdf.parse(dateString)
            time = d!!.time
        } catch (e: Exception) {

        }
        return time

    }

    fun getMonthPlusOne(month: Int): String {
        val mMonth = month + 1
        return if (mMonth < 10) "0$mMonth" else mMonth.toString()
    }

    fun getMonth(month: Int): String {
        return if (month < 10) "0$month" else month.toString()
    }

    fun convertStringToInt(value: Editable): Int {
        val mString = value.toString()
        return if (mString.trim().isEmpty()) 0 else mString.toInt()
    }

    fun getQRCode(context: Context, content: String): Bitmap? {
        val hintsMap: MutableMap<EncodeHintType, Any?> = HashMap()
        hintsMap[EncodeHintType.CHARACTER_SET] = "utf-8"
        hintsMap[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.Q
        hintsMap[EncodeHintType.MARGIN] = 1

        val white: Int = Color.WHITE
        val black: Int = Color.BLACK

        val mHeight = (context.getResources().getDisplayMetrics().heightPixels / 2.4).toInt()
        val mWidth = (context.getResources().getDisplayMetrics().widthPixels / 1.3).toInt()

        try {

            val bitMatrix =
                QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, mWidth, mHeight, hintsMap)
            val pixels = IntArray(mWidth * mHeight)
            for (i in 0 until mHeight) {
                val offset: Int = i * mWidth
                for (j in 0 until mWidth) {
                    pixels[offset + j] = if (bitMatrix[j, i]) black else white
                }
            }
            return Bitmap.createBitmap(pixels, mWidth, mHeight, Bitmap.Config.ARGB_4444)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
        return null
    }

    fun bitmapToFile(bitmap: Bitmap, fileNameToSave: String): File? { // File name like "image.png"
        //create a file to write bitmap data
        var file: File? = null
        return try {
            file = File(Environment.getDownloadCacheDirectory().toString() + File.separator + fileNameToSave + ".jpeg")
            file.createNewFile()

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 0, bos) // YOU can also save it in JPEG
            val bitmapData = bos.toByteArray()

            //write the bytes in file
            val fos = FileOutputStream(file)
            fos.write(bitmapData)
            fos.flush()
            fos.close()
            file
        } catch (e: Exception) {
            e.printStackTrace()
            file // it will return null
        }

    }

    // Method to save an image to external storage
    fun saveImageToExternalStorage(context: Context, bitmap:Bitmap, name: String):Uri{
        // Get the external storage directory path

        val path = Environment.getExternalStorageDirectory().toString()

        // Create a file to save the image
        val file = File(path, "${name.replace(" ", "_")}.jpg")

        try {
            // Get the file output stream
            if (!file.exists()) {
                val stream: OutputStream = FileOutputStream(file)

                // Compress the bitmap
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)

                // Flush the output stream
                stream.flush()

                // Close the output stream
                stream.close()
            }
        } catch (e: IOException){ // Catch the exception
            e.printStackTrace()
        }

        // Return the saved image path to uri
        return Uri.parse(file.absolutePath)
    }

    fun storeImage(bitmap: Bitmap, imageName: String) : String {
        var filePath = ""
        val fullImageName = "$imageName.jpeg"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val uri = storeImageToScopedStorage(fullImageName, ImageUtils.IMAGES_FOLDER, bitmap)
            if (uri != null) filePath = uri.toString()
        } else {
            val imagesFolderPath = Environment.getExternalStorageDirectory().absolutePath + "/" + ImageUtils.IMAGES_FOLDER
            val imagesDir = File(imagesFolderPath)
            if (!imagesDir.exists()) {
                if (!imagesDir.mkdirs()) {
                    Log.w(Toolbox::class.java, "Failed to create folder $imagesFolderPath")
                }
            }
            filePath = "$imagesFolderPath/$fullImageName"
            val b = bitmap
            var fos: FileOutputStream? = null
            try {
                fos = FileOutputStream(filePath)
                val success = b.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                if (!success) {
                    Log.e(ImageUtils::class.java, "Failed to compress bitmap!")
                    try {
                        fos.close()
                    } catch (ignored: IOException) {
                    } finally {
                        fos = null
                    }
                    val deleteSuccess = File(filePath).delete()
                    if (!deleteSuccess) {
                        Log.e(ImageUtils::class.java, "Failed to delete {}", filePath)
                    }
                }
            } catch (e: FileNotFoundException) {
                Log.e(ImageUtils::class.java, e, "Failed to save image")
            } finally {
                if (fos != null) {
                    try {
                        fos.close()
                    } catch (ignored: IOException) {
                    }
                }
            }
        }
        return filePath
    }

    private fun storeImageToScopedStorage(
        imageName: String,
        imageFolder: String,
        bitmap: Bitmap) : Uri? {
        val context = MicroblinkSDK.getApplicationContext()
        val contentValues = ContentValues()
        contentValues.put(MediaStore.Files.FileColumns.DISPLAY_NAME, imageName)
        contentValues.put(MediaStore.Files.FileColumns.MIME_TYPE, "image/jpeg")
        contentValues.put(MediaStore.Files.FileColumns.RELATIVE_PATH, imageFolder)
        contentValues.put(MediaStore.Files.FileColumns.IS_PENDING, 1)
        val contentResolver = context!!.contentResolver
        val collection = MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
        val uri = contentResolver.insert(collection, contentValues)
        try {
            val stream = contentResolver.openOutputStream(uri!!)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } finally {
            contentValues.clear()
            contentValues.put(MediaStore.Files.FileColumns.IS_PENDING, 0)
            contentResolver.update(uri!!, contentValues, null, null)
            return uri
        }
    }

}