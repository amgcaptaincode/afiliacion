package com.afiliacion.mx.utils

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import com.microblink.MicroblinkSDK
import com.microblink.image.Image
import com.microblink.image.highres.HighResImageWrapper
import com.microblink.util.Log
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object ImageUtils {

    const val IMAGES_FOLDER = "Documents/AfiliacionMx"
    private const val HIGH_RES_IMAGES_FOLDER = "$IMAGES_FOLDER/HighRes"
    private const val FRAMES_FOLDER = "$IMAGES_FOLDER/Frames"


    fun storeHighResImage(
        context: Context,
        imageName: String,
        image: HighResImageWrapper
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            storeImageToScopedStorage(
                imageName,
                HIGH_RES_IMAGES_FOLDER,
                image.image.convertToBitmap()!!
            )
            Toast.makeText(context,
                "Saved to $HIGH_RES_IMAGES_FOLDER", Toast.LENGTH_SHORT).show()
        } else {
            val imagesFolderPath =
                Environment.getExternalStorageDirectory()
                    .absolutePath + "/" + HIGH_RES_IMAGES_FOLDER
            val imagesDir = File(imagesFolderPath)
            if (!imagesDir.exists()) {
                imagesDir.mkdirs()
            }
            val imagePath = "$imagesFolderPath/$imageName"
            val file = File(imagePath)
            try {
                image.saveToFile(file)
                MediaScannerConnection.scanFile(
                    context,
                    arrayOf(imagePath),
                    null,
                    null
                )
                Toast.makeText(
                    context, "Saved to $HIGH_RES_IMAGES_FOLDER", Toast.LENGTH_SHORT).show()
            } catch (e: IOException) {
                e.printStackTrace()
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun storeImage(image: Image, imageName: String) {
        val dateFormat: DateFormat =
            SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US)
        val dateString = dateFormat.format(Date())
        val fullImageName = "$imageName-$dateString.jpg"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            storeImageToScopedStorage(
                fullImageName,
                ImageUtils.IMAGES_FOLDER,
                image.convertToBitmap()!!
            )
        } else {
            val imagesFolderPath = Environment.getExternalStorageDirectory().absolutePath + "/" + ImageUtils.IMAGES_FOLDER
            val imagesDir = File(imagesFolderPath)
            if (!imagesDir.exists()) {
                if (!imagesDir.mkdirs()) {
                    Log.w(ImageUtils::class.java, "Failed to create folder $imagesFolderPath")
                }
            }
            val filename = "$imagesFolderPath/$fullImageName"
            val b = image.convertToBitmap()
            if (b == null) {
                Log.e(ImageUtils::class.java, "Failed to convert image to bitmap!")
                return
            }
            var fos: FileOutputStream? = null
            try {
                fos = FileOutputStream(filename)
                val success = b.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                if (!success) {
                    Log.e(ImageUtils::class.java, "Failed to compress bitmap!")
                    try {
                        fos.close()
                    } catch (ignored: IOException) {
                    } finally {
                        fos = null
                    }
                    val deleteSuccess = File(filename).delete()
                    if (!deleteSuccess) {
                        Log.e(ImageUtils::class.java, "Failed to delete {}", filename)
                    }
                }
            } catch (e: FileNotFoundException) {
                Log.e(ImageUtils::class.java, e, "Failed to save image")
            } finally {
                if (fos != null) {
                    try {
                        fos.close()
                    } catch (ignored: IOException) {
                    }
                }
            }
        }
    }


    private fun storeImageToScopedStorage(
        imageName: String,
        imageFolder: String,
        bitmap: Bitmap
    ) {
        val context = MicroblinkSDK.getApplicationContext()
        val contentValues = ContentValues()
        contentValues.put(MediaStore.Files.FileColumns.DISPLAY_NAME, imageName)
        contentValues.put(MediaStore.Files.FileColumns.MIME_TYPE, "image/jpeg")
        contentValues.put(MediaStore.Files.FileColumns.RELATIVE_PATH, imageFolder)
        contentValues.put(MediaStore.Files.FileColumns.IS_PENDING, 1)
        val contentResolver = context!!.contentResolver
        val collection = MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
        val uri = contentResolver.insert(collection, contentValues)
        try {
            val stream = contentResolver.openOutputStream(uri!!)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } finally {
            contentValues.clear()
            contentValues.put(MediaStore.Files.FileColumns.IS_PENDING, 0)
            contentResolver.update(uri!!, contentValues, null, null)
        }
    }

}