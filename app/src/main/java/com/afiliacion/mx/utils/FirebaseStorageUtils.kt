package com.afiliacion.mx.utils

import android.graphics.Bitmap
import android.util.Log
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.io.ByteArrayOutputStream

object FirebaseStorageUtils {

    const val TAG = "FirebaseStorageUtils"

    fun uploadFile(fileName: String, folder: String, bitmap: Bitmap) {

        val storageRef = Firebase.storage.reference

        val fileRef = storageRef.child("$folder/$fileName.jpeg")

        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        val uploadTask = fileRef.putBytes(data)

        uploadTask.addOnSuccessListener {
            Log.d(TAG, it.metadata!!.path)
        }.addOnFailureListener {
            Log.e(TAG, "Error", it)
        }


    }

}