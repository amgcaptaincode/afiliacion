package com.afiliacion.mx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.afiliacion.mx.data.repository.AfiliacionRepository
import com.afiliacion.mx.model.entities.DataIne

class MainViewModel : ViewModel() {

    private val afiliacionRepository = AfiliacionRepository()
    private var TAG = "MainViewModel"

    private val _savedData = MutableLiveData<Boolean>()
    val savedData: LiveData<Boolean>
        get() = _savedData

    val _dataIne = MutableLiveData<DataIne>()

    fun setDataIne(dataIne: DataIne) {
        _dataIne.value = dataIne
    }

    fun getDataIne() = _dataIne.value

    fun saveDataIne(dataIne: DataIne) {

        afiliacionRepository.saveDataIne(dataIne)
            .addOnSuccessListener {
                Log.d(TAG, "Datos escritos correctamente en devices")
                setDataIne(dataIne)
                _savedData.value = true
            }.addOnFailureListener { e ->
                Log.w(TAG, "Error al guardar datos ", e)
                _savedData.value = false
            }


    }


}