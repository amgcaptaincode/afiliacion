package com.afiliacion.mx.ui.dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import java.util.*

class DatePickerFragment : DialogFragment() {

    private var listener: DatePickerDialog.OnDateSetListener? = null
    private var initialYear: Int = -1
    private var initialMonth: Int = -1
    private var initialDay: Int = -1
    private var minYear: Int = -1
    private var maxYear: Int = -1

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current date as the default date in the picker
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // Initial selected value
        if (initialYear == -1)
            initialYear = year

        if (initialMonth == -1)
            initialMonth = month

        if (initialDay == -1)
            initialDay = day

        val datePickerDialog = DatePickerDialog(requireContext(), listener, initialYear, initialMonth, initialDay)

        // Min and max date
        /*if (minYear == -1) {
            c.set(Calendar.YEAR, year - 100)
        } else {
            c.set(Calendar.YEAR, year - minYear)
        }
        datePickerDialog.datePicker.minDate = c.timeInMillis

        if (maxYear == -1) {
            c.set(Calendar.YEAR, year)
        } else {
            c.set(Calendar.YEAR, maxYear)
        }
        datePickerDialog.datePicker.maxDate = c.timeInMillis*/

        // Create a new instance of DatePickerDialog and return it
        return datePickerDialog
    }

    companion object {

        fun newInstance(listener: DatePickerDialog.OnDateSetListener, year: Int, month: Int, day: Int): DatePickerFragment {
            val fragment = DatePickerFragment();
            fragment.listener = listener
            fragment.initialYear = year
            fragment.initialMonth = month
            fragment.initialDay = day
            return fragment
        }

    }

}