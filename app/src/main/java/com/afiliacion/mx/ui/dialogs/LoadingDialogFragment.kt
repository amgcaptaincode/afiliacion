package com.afiliacion.mx.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.afiliacion.mx.R

class LoadingDialogFragment : DialogFragment() {

    companion object {
        const val TAG = ""

        fun newInstance(): LoadingDialogFragment {
            return LoadingDialogFragment()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = false
        return inflater.inflate(R.layout.dialog_loading, container, false)
    }

    override fun getTheme(): Int {
        return R.style.DialogTheme_Loading
    }


}