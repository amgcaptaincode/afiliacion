package com.afiliacion.mx.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afiliacion.mx.databinding.ItemEventBinding
import com.afiliacion.mx.model.entities.Event

class EventsAdapter(items: ArrayList<Event>) : BaseAdapter<Event>(items) {
    override fun setViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Event> {
        val binding: ItemEventBinding =
            ItemEventBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EventsViewHolder(binding, binding.root)
    }

    class EventsViewHolder(
        private val itemEventBinding: ItemEventBinding,
        override val containerView: View?
    ) :
        BaseViewHolder<Event>(itemEventBinding.root) {

        override fun bind(data: Event) {
            itemEventBinding.tvNameEvent.text = data.name
            itemEventBinding.tvDescriptionEvent.text = data.description
            itemEventBinding.tvLocationEvent.text = data.location
            itemEventBinding.tvDateEvent.text = data.dateTime.toString()
        }

    }

}