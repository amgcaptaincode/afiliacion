package com.afiliacion.mx.ui.dialogs

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.NumberPicker
import androidx.fragment.app.DialogFragment
import com.afiliacion.mx.R
import java.util.*


class MonthYearPickerDialog : DialogFragment() {

    private val MAX_YEAR = 2099
    private var listener: DatePickerDialog.OnDateSetListener? = null
    private var mShowMonth = true

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater

        val cal: Calendar = Calendar.getInstance()

        val dialog: View = inflater.inflate(R.layout.month_year_picker_dialog, null)
        val monthPicker = dialog.findViewById<NumberPicker>(R.id.picker_month)
        val yearPicker = dialog.findViewById<NumberPicker>(R.id.picker_year)

        if (!mShowMonth)
            monthPicker.visibility = View.GONE

        monthPicker.minValue = 1
        monthPicker.maxValue = 12
        monthPicker.value = cal.get(Calendar.MONTH) + 1

        val year: Int = cal.get(Calendar.YEAR)
        yearPicker.minValue = 1900
        yearPicker.maxValue = MAX_YEAR
        yearPicker.value = year

        builder.setView(dialog).setPositiveButton("Aceptar",
            DialogInterface.OnClickListener { dialog, id ->
                listener!!.onDateSet(
                    null,
                    yearPicker.value,
                    monthPicker.value,
                    0
                )
            }).setNegativeButton("Cancelar",
            DialogInterface.OnClickListener { dialog, id -> this@MonthYearPickerDialog.dialog!!.cancel() })
        return builder.create()
    }

    companion object {
        fun newInstance(showMonth: Boolean, listener: DatePickerDialog.OnDateSetListener): MonthYearPickerDialog {
            val fragment = MonthYearPickerDialog();
            fragment.listener = listener
            fragment.mShowMonth = showMonth
            return fragment
        }
    }


}