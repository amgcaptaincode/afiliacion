package com.afiliacion.mx.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.afiliacion.mx.ui.fragments.EventFragment
import com.afiliacion.mx.MainViewModel
import com.afiliacion.mx.R
import com.afiliacion.mx.databinding.ActivityMainBinding
import com.afiliacion.mx.ui.fragments.ScanIneFragment
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbarMain)
        supportActionBar!!.title = ""

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        val scanIneFragment = ScanIneFragment()
        val eventFragment = EventFragment()

        setCurrentFragment(scanIneFragment)

        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.scanner -> setCurrentFragment(scanIneFragment)
                R.id.event -> setCurrentFragment(eventFragment)
            }
            true
        }

    }

    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(binding.flFragment.id, fragment)
            commit()
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.sign_off -> {
                // cerrar sesion
                signOff()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun signOff() {
        Firebase.auth.signOut()
        val intent = Intent(this, SplashActivity::class.java)
        startActivity(intent)
        finish()
    }

}