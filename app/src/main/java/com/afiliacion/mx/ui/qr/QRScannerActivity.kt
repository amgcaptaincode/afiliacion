package com.afiliacion.mx.ui.qr

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.afiliacion.mx.constants.AppConstants
import com.afiliacion.mx.databinding.ActivityQrScannerBinding
import com.afiliacion.mx.interfaces.GenericListener
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView


class QRScannerActivity : AppCompatActivity(), ZXingScannerView.ResultHandler, GenericListener {
    
    private lateinit var binding: ActivityQrScannerBinding
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQrScannerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        checkPermission(Manifest.permission.CAMERA, AppConstants.CAMERA_PERMISSION_CODE)

    }

    // Function to check and request permission 
    private fun checkPermission(permission: String, requestCode: Int) {

        // Checking if permission is not granted 
        if (ContextCompat.checkSelfPermission(
                this@QRScannerActivity,
                permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(
                this@QRScannerActivity, arrayOf(permission),
                requestCode
            )
        } else {
            startScanner()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == AppConstants.CAMERA_PERMISSION_CODE) {

            // Checking whether user granted the permission or not. 
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startScanner()
            } else {
                showDialogMessagePermission("Permiso para usar la cámara es necesario.")
            }
        }
    }

    private fun startScanner() {
        binding.qrScanner.setResultHandler(this) // Register ourselves as a handler for scan results.
        binding.qrScanner.startCamera() // Start camera on resume
        binding.qrScanner.setAutoFocus(true)

        val formats : MutableList<BarcodeFormat> = mutableListOf()
        formats.add(BarcodeFormat.QR_CODE)

        binding.qrScanner.setFormats(formats)
        binding.qrScanner.setBorderColor(Color.GREEN)
        binding.qrScanner.setBorderStrokeWidth(8)
        binding.qrScanner.setLaserEnabled(true)
        binding.qrScanner.setMaskColor(Color.TRANSPARENT)

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.qrScanner.stopCamera()
    }

    override fun handleResult(result: Result) {
        // TODO Revisar como manejar el resultado
        Log.e("", "Result ${result.text}}")
        showWarningDialog(result.text)
    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun showWarningDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Aviso")
        builder.setMessage(message)

        builder.setPositiveButton("Aceptar") { dialog, which ->
            binding.qrScanner.stopCamera()
            startScanner()
        }
        builder.show()
    }

    private fun showDialogMessagePermission(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Aviso")
        builder.setMessage(message)

        builder.setPositiveButton("Aceptar") { dialog, which ->
            checkPermission(Manifest.permission.CAMERA, AppConstants.CAMERA_PERMISSION_CODE)
        }
        builder.setNegativeButton("Salir") {  dialog, which ->
            finish()
        }
        builder.show()
    }


}