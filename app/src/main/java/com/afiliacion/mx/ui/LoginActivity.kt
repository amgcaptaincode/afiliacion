package com.afiliacion.mx.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.afiliacion.mx.interfaces.GenericListener
import com.afiliacion.mx.databinding.ActivityLoginBinding
import com.afiliacion.mx.ui.dialogs.LoadingDialogFragment
import com.afiliacion.mx.utils.Toolbox
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import java.util.concurrent.TimeUnit
import com.afiliacion.mx.R
import com.afiliacion.mx.constants.AppConstants
import com.afiliacion.mx.utils.ExtensionEditText.afterTextChanged

class LoginActivity : AppCompatActivity(),
    GenericListener {

    private val TAG: String = "LoginActivity"

    private lateinit var binding: ActivityLoginBinding
    private lateinit var mAuth: FirebaseAuth
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private lateinit var mVerificationId: String
    private var codeSent: Boolean = false

    private var mWait = LoadingDialogFragment.newInstance()

    private lateinit var countDownTimer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        init()

        initCallback()

        initListener()
    }

    private fun init() {

        mAuth = FirebaseAuth.getInstance()
        //mAuth.firebaseAuthSettings.setAppVerificationDisabledForTesting(true)

        //mAuth.languageCode = "es"

        binding.btnContinue.text = getString(R.string.label_send_codee)

        countDownTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val seconds = (millisUntilFinished / 1000L)
                binding.tvWaitTime.text = getString(R.string.label_wait_time, seconds)
            }

            override fun onFinish() {
                codeSent = false
                binding.btnContinue.text = getString(R.string.label_resend_code)
                binding.tvWaitTime.visibility = View.GONE
            }

        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initListener() {

        binding.root.setOnTouchListener { view, motionEvent ->
            Toolbox.closeKeyBoard(this)
            return@setOnTouchListener false
        }

        binding.btnContinue.setOnClickListener {
            if (codeSent) {
                checkCode()
            } else {
                sendCode()
            }
        }

        binding.editTextPhoneNumber.setOnEditorActionListener(TextView.OnEditorActionListener { textView, id, keyEvent ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                sendCode()
                return@OnEditorActionListener true
            }
            false
        })

        binding.editTextVerificationCode.setOnEditorActionListener(TextView.OnEditorActionListener { textView, id, keyEvent ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                checkCode()
                return@OnEditorActionListener true
            }
            false
        })


        binding.editTextPhoneNumber.afterTextChanged { checkText(it) }

    }

    private fun sendCode() {
        var mobile = binding.editTextPhoneNumber.text.toString().trim()

        if (mobile.isEmpty() || mobile.length < 10) {
            binding.editTextPhoneNumber.setError("Ingresa un número valido")
            binding.editTextPhoneNumber.requestFocus()
        } else {
            mobile = "+52".plus(mobile)
            sendVerificationCode(mobile)
        }
    }

    private fun checkCode() {
        val verificationCode = binding.editTextVerificationCode.text.toString().trim()

        if (verificationCode.isEmpty()) {
            binding.editTextVerificationCode.setError("Ingresa un código valido")
            binding.editTextVerificationCode.requestFocus()
        } else {
            verifyVerificationCode(verificationCode)
        }
    }

    private fun startTimeCounter() {

        binding.btnContinue.text = getString(R.string.label_verify_code)
        binding.tvWaitTime.visibility = View.VISIBLE

        countDownTimer.start()
    }

    private fun cancelTimeCounter() {
        binding.tvWaitTime.visibility = View.GONE
        countDownTimer.cancel()
    }

    private fun checkText(it: String) {

    }


    private fun sendVerificationCode(mobile: String) {
        // TODO Mostrar timer de 1 minuto
        //showLoading()

        val options = PhoneAuthOptions.newBuilder(mAuth)
            .setPhoneNumber(mobile)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()

        PhoneAuthProvider.verifyPhoneNumber(options)
        Toast.makeText(this, "Se ha enviado un código por SMS", Toast.LENGTH_LONG).show()
        binding.textInputLayoutVerificationCode.isEnabled = true
        binding.editTextPhoneNumber.clearFocus()
        binding.editTextVerificationCode.requestFocus()
        codeSent = true
        startTimeCounter()
    }

    private fun initCallback() {
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                Log.d(TAG, "onVerificationCompleted:$credential")

                val code = credential.smsCode

                if (code != null) {
                    binding.editTextVerificationCode.setText(code)
                    verifyVerificationCode(code)
                } else {
                    cancelTimeCounter()
                }

                //signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Log.w(TAG, "onVerificationFailed", e)
                hideLoading()
                binding.btnContinue.text = getString(R.string.label_resend_code)
                binding.textInputLayoutVerificationCode.isEnabled = false
                cancelTimeCounter()
                Snackbar.make(binding.root, e.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                super.onCodeSent(verificationId, token)
                hideLoading()
                Log.d(TAG, "onCodeSent:$verificationId")
                mVerificationId = verificationId
            }

        }
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                cancelTimeCounter()
                hideLoading()
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")

                    val user = task.result?.user
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                } else {

                    var message = "Ha ocurrido un error"

                    // Sign in failed, display a message and update the UI
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        message = "Código invalido"
                    }
                    // TODO falta opcion para reenviar código
                    Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
                }
            }
    }

    private fun verifyVerificationCode(code: String) {
        showLoading()
        val credential = PhoneAuthProvider.getCredential(mVerificationId, code)

        signInWithPhoneAuthCredential(credential)

    }

    private fun isSafeShowProgress(): Boolean {
        return mWait != null && !mWait.isVisible
    }

    override fun showLoading() {
        if (isSafeShowProgress()) mWait.show(supportFragmentManager, LoadingDialogFragment.TAG)
    }

    override fun hideLoading() {
        if (mWait != null && mWait.isVisible) mWait.dismiss()
    }

    override fun showWarningDialog(message: String) {

    }


}