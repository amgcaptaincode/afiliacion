package com.afiliacion.mx.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.afiliacion.mx.MainViewModel
import com.afiliacion.mx.R
import com.afiliacion.mx.constants.AppConstants
import com.afiliacion.mx.databinding.FragmentScanIneBinding
import com.afiliacion.mx.interfaces.GenericListener
import com.afiliacion.mx.model.entities.DataIne
import com.afiliacion.mx.ui.dialogs.DatePickerFragment
import com.afiliacion.mx.ui.dialogs.GenerateQRDialog
import com.afiliacion.mx.ui.dialogs.LoadingDialogFragment
import com.afiliacion.mx.ui.dialogs.MonthYearPickerDialog
import com.afiliacion.mx.utils.FirebaseStorageUtils
import com.afiliacion.mx.utils.Toolbox
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.microblink.entities.recognizers.Recognizer
import com.microblink.entities.recognizers.RecognizerBundle
import com.microblink.entities.recognizers.blinkid.generic.BlinkIdCombinedRecognizer
import com.microblink.entities.recognizers.successframe.SuccessFrameGrabberRecognizer
import com.microblink.image.Image
import com.microblink.uisettings.ActivityRunner
import com.microblink.uisettings.BlinkIdUISettings

class ScanIneFragment : Fragment(), GenericListener {

    private var _binding: FragmentScanIneBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by activityViewModels()

    private val REQUEST_CODE_BLINK: Int = 242

    private lateinit var recognizer: BlinkIdCombinedRecognizer
    private lateinit var recognizerBundle: RecognizerBundle

    private var dataIne: DataIne = DataIne()
    private var mWait = LoadingDialogFragment.newInstance()

    private var user: FirebaseUser? = Firebase.auth.currentUser

    private var bitmapFrontImage : Bitmap? = null
    private var bitmapBackImage : Bitmap? = null
    private var bitmapFaceImage : Bitmap? = null
    private var bitmapSignatureImage : Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentScanIneBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        initListeners()

        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, AppConstants.STORAGE_PERMISSION_CODE)
    }

    private fun initViews() {

        if (user == null)
            requireActivity().finish()

        // setup views, as you would normally do in onCreate callback

        // create BlinkIdCombinedRecognizer for scanning supported document
        // see https://bit.ly/3byPtoN for options
        recognizer = BlinkIdCombinedRecognizer()

        recognizer.setReturnFullDocumentImage(true)
        recognizer.setReturnFaceImage(true)
        recognizer.setReturnSignatureImage(true)

        val successFrameGrabberRecognizer = SuccessFrameGrabberRecognizer(recognizer)

        // bundle recognizers into RecognizerBundle
        recognizerBundle = RecognizerBundle(successFrameGrabberRecognizer)

    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initListeners() {
        binding.btnStartScanner.setOnClickListener {
            startScanning()
        }

        binding.btnSaveData.setOnClickListener {
            savedData()
        }

        viewModel.savedData.observe(viewLifecycleOwner, Observer {
            hideLoading()
            if (it) {
                // Datos guardados correctamente
                showGenerateQRDialog()
            } else {
                // Datos no guardados
                Toast.makeText(requireContext(), "Datos no guardados", Toast.LENGTH_SHORT).show()
            }
        })

        binding.editTextBirthday.setOnClickListener {
            showDialogPicker()
        }

        binding.editTextAnioRegistro.setOnClickListener {
            showDialogPickerMonthYear()
        }

        binding.editTextEmission.setOnClickListener {
            showDialogPickerYear(it as TextInputEditText)
        }

        binding.editTextValidity.setOnClickListener {
            showDialogPickerYear(it as TextInputEditText)
        }

        binding.nsvContainer.setOnTouchListener { v, _ ->
            val imm: InputMethodManager =
                requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(v.windowToken, 0)
            false
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                permission) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(arrayOf(permission), requestCode)
        } else {
            startScanning()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == AppConstants.STORAGE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startScanning()
            } else {
                showDialogMessagePermission()
            }
        }
    }

    fun startScanning() {
        clearForm(binding.llContainer)
        val settings = BlinkIdUISettings(recognizerBundle)

        settings.enableHighResSuccessFrameCapture(true)
        ActivityRunner.startActivityForResult(this, REQUEST_CODE_BLINK, settings)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE_BLINK) {
            if (resultCode == Activity.RESULT_OK && data != null) {

                // load the data into all recognizers bundled within your RecognizerBundle
                recognizerBundle.loadFromIntent(data)

                analyzeResult(data, recognizer.result)
            }
        }
    }

    private fun analyzeResult(data: Intent, mResult: BlinkIdCombinedRecognizer.Result) {

        if (mResult.resultState == Recognizer.Result.State.Valid) {

            val firstRecognizer = recognizerBundle.recognizers[0]
            val successFrameGrabberRecognizer = firstRecognizer as SuccessFrameGrabberRecognizer
            //get wrapped recognizer
            val recognizer = successFrameGrabberRecognizer.slaveRecognizer as BlinkIdCombinedRecognizer
            val result = recognizer.result

            if (checkImage(result.fullDocumentFrontImage)) {
                bitmapFrontImage = result.fullDocumentFrontImage!!.convertToBitmap()!!
                showImage(bitmapFrontImage, binding.ivFrontImage)
            }

            if (checkImage(result.fullDocumentBackImage)) {
                bitmapBackImage = result.fullDocumentBackImage!!.convertToBitmap()!!
                showImage(result.fullDocumentBackImage!!.convertToBitmap(), binding.ivBackImage)
            }

            if (checkImage(result.faceImage)) {
                bitmapFaceImage = result.faceImage!!.convertToBitmap()!!
            }

            if (checkImage(result.signatureImage)) {
                bitmapSignatureImage = result.signatureImage!!.convertToBitmap()!!
            }

            binding.llContainer.visibility = View.VISIBLE

            val name = result.fullName.replace("\n", " ")
            val fechaNacimiento = result.dateOfBirth.date
            val edad = result.age
            val caducidad = result.dateOfExpiry.date
            val noDoc = result.documentNumber
            val claveElector = result.documentAdditionalNumber
            val curp = result.personalIdNumber
            val anioRegistro = result.documentOptionalAdditionalNumber
            val direccion = result.address
            val textoMrz = result.mrzResult.mrzText
            val sexo = result.sex

            binding.editTextName.setText(name)
            binding.editTextDocumentNumber.setText(noDoc)
            binding.editTextClaveElector.setText(claveElector)
            binding.editTextCurp.setText(curp)
            binding.editTextAnioRegistro.setText(anioRegistro)
            binding.editTextAddress.setText(direccion)
            binding.radioButtonMen.isChecked = sexo.equals("H")
            binding.radioButtonWomen.isChecked = sexo.equals("M")

            val vigencia = caducidad?.year ?: 0

            binding.editTextValidity.setText(vigencia.toString())

            var fechaNacimientoTime = 0L
            if (fechaNacimiento != null) {
                val mMonth = Toolbox.getMonth(fechaNacimiento.month)
                val dateString = "${fechaNacimiento.day}/${mMonth}/${fechaNacimiento.year}"

                fechaNacimientoTime = Toolbox.convertDateToTimestamp(dateString)
                binding.editTextBirthday.setText(dateString)
            }

            dataIne = DataIne(
                name,
                fechaNacimientoTime,
                sexo,
                direccion,
                claveElector,
                curp,
                anioRegistro.toInt(),
                0,
                0,
                0,
                0,
                0,
                vigencia,
                "",
                "",
                "",
                true,
                noDoc
            )

        } else {
            // TODO validar que hacer cuando no hay resultados
        }
    }

    private fun showImage(bitmap: Bitmap?, imageView: ImageView) {
        Glide.with(requireContext())
            .load(bitmap)
            .centerInside()
            .into(imageView)
        imageView.visibility = View.VISIBLE
    }

    private fun checkImage(image: Image?): Boolean {
        if (image != null) {
            if (image.convertToBitmap() != null) {
                return true
            }
        }
        return false
    }

    private fun savedData() {
        val isValidField = checkFields()

        if (isValidField) {
            showLoading()

            val dateString = binding.editTextBirthday.text.toString()
            val fechaNacimientoTime = Toolbox.convertDateToTimestamp(dateString)

            dataIne.nombre = binding.editTextName.text.toString().trim()
            dataIne.fechaNacimiento = fechaNacimientoTime
            dataIne.sexo = if (binding.radioButtonMen.isChecked) "H" else if (binding.radioButtonWomen.isChecked) "M" else ""
            dataIne.domicilio = binding.editTextAddress.text.toString().trim()
            dataIne.claveElector = binding.editTextClaveElector.text.toString().trim()
            dataIne.curp = binding.editTextCurp.text.toString().trim()
            dataIne.anioRegistro = Toolbox.convertStringToInt(binding.editTextAnioRegistro.text!!)
            dataIne.estado = Toolbox.convertStringToInt(binding.editTextState.text!!)
            dataIne.municipio = Toolbox.convertStringToInt(binding.editTextMunicipality.text!!)
            dataIne.seccion = Toolbox.convertStringToInt(binding.editTextSection.text!!)
            dataIne.localidad = Toolbox.convertStringToInt(binding.editTextLocality.text!!)
            dataIne.emision = Toolbox.convertStringToInt(binding.editTextEmission.text!!)
            dataIne.vigencia = Toolbox.convertStringToInt(binding.editTextValidity.text!!)
            dataIne.creador = user!!.phoneNumber

            uploadImages(dataIne.claveElector!!)

            viewModel.saveDataIne(dataIne)
        } else {
            showWarningDialog(getString(R.string.label_check_information))
        }
    }

    private fun uploadImages(electorKey: String) {
        if (bitmapFrontImage != null) {
            FirebaseStorageUtils.uploadFile(
                "anverso",
                electorKey,
                bitmapFrontImage!!
            )
        }

        if (bitmapBackImage != null) {
            FirebaseStorageUtils.uploadFile(
                "reverso",
                electorKey,
                bitmapBackImage!!
            )
        }

        if (bitmapFaceImage != null) {
            FirebaseStorageUtils.uploadFile(
                "cara",
                electorKey,
                bitmapFaceImage!!
            )
        }

        if (bitmapSignatureImage != null) {
            FirebaseStorageUtils.uploadFile(
                "firma",
                electorKey,
                bitmapSignatureImage!!
            )
        }
    }

    private fun checkFields(): Boolean {
        val isValidName = checkField(binding.editTextName.text.toString().trim())
        val isValidBirthday = checkField(binding.editTextBirthday.text.toString().trim())
        val isValidAddress = checkField(binding.editTextAddress.text.toString().trim())
        val isValidClaveElector = checkField(binding.editTextClaveElector.text.toString().trim())
        val isValidCurp = checkField(binding.editTextCurp.text.toString().trim())
        val isValidAnioRegistro = checkField(binding.editTextAnioRegistro.text.toString().trim())
        val isValidState = checkField(binding.editTextState.text.toString().trim())
        val isValidMunicipality = checkField(binding.editTextMunicipality.text.toString().trim())
        val isValidSection = checkField(binding.editTextSection.text.toString().trim())
        val isValidLocality = checkField(binding.editTextLocality.text.toString().trim())
        val isValidEmission = checkField(binding.editTextEmission.text.toString().trim())
        val isValidValidity = checkField(binding.editTextValidity.text.toString().trim())

        return (isValidName && isValidBirthday && isValidAddress && isValidClaveElector
                && isValidCurp && isValidAnioRegistro && isValidState && isValidMunicipality
                && isValidSection && isValidLocality && isValidEmission && isValidValidity && checkRadios())

    }

    private fun checkField(value: String) : Boolean = value.isNotEmpty()

    private fun checkRadios(): Boolean {
        return (binding.radioButtonMen.isChecked || binding.radioButtonWomen.isChecked)
    }

    private fun showDialogPicker() {
        val datePickerDialog = DatePickerFragment.newInstance(
            DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                val mMonth = Toolbox.getMonthPlusOne(month)
                val selectedDate = "$day/$mMonth/$year"
                binding.editTextBirthday.setText(selectedDate)
            }, -1, -1, -1
        )

        datePickerDialog.show(requireActivity().supportFragmentManager, "datePicker")

    }

    private fun createDialogWithoutDateField(): DatePickerDialog? {
        val dpd = DatePickerDialog(requireContext(), null, 2014, 1, 24)
        try {
            val datePickerDialogFields =
                dpd.javaClass.declaredFields
            for (datePickerDialogField in datePickerDialogFields) {
                if (datePickerDialogField.name == "mDatePicker") {
                    datePickerDialogField.isAccessible = true
                    val datePicker = datePickerDialogField[dpd] as DatePicker
                    val datePickerFields =
                        datePickerDialogField.type.declaredFields
                    for (datePickerField in datePickerFields) {
                        Log.i("test", datePickerField.name)
                        if ("mDaySpinner" == datePickerField.name) {
                            datePickerField.isAccessible = true
                            val dayPicker = datePickerField[datePicker]
                            (dayPicker as View).visibility = View.GONE
                        }
                    }
                }
            }
        } catch (ex: Exception) {
        }
        return dpd
    }

    private fun showDialogPickerMonthYear() {
        val monthYearPickerDialog =
            MonthYearPickerDialog.newInstance(true, DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                val mMonth = Toolbox.getMonth(month)
                binding.editTextAnioRegistro.setText(year.toString().plus(mMonth))
            })
        monthYearPickerDialog.show(requireActivity().supportFragmentManager, "mDatePicker")
    }

    private fun showDialogPickerYear(ediText: TextInputEditText) {
        val monthYearPickerDialog =
            MonthYearPickerDialog.newInstance(false, DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                val mMonth = Toolbox.getMonth(month)
                ediText.setText(year.toString())
            })
        monthYearPickerDialog.show(requireActivity().supportFragmentManager, "mDatePicker")
    }

    private fun clearForm(group: ViewGroup) {
        var i = 0
        val count = group.childCount
        while (i < count) {
            val view = group.getChildAt(i)
            if (view is EditText) {
                view.setText("")
            } else if(view is ImageView) {
                view.setImageDrawable(null)
                view.visibility = View.GONE
            } else if (view is RadioButton) {
                view.isChecked = false
            }
            if (view is ViewGroup && view.childCount > 0) clearForm(view)
            ++i
            view.clearFocus()
        }
    }

    override fun showLoading() {
        if (isSafeShowProgress()) mWait.show(
            requireActivity().supportFragmentManager,
            LoadingDialogFragment.TAG
        )
    }

    override fun hideLoading() {
        if (mWait != null && mWait.isVisible) mWait.dismiss()
    }

    override fun showWarningDialog(message: String) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Aviso")
        builder.setMessage(message)

        builder.setPositiveButton("Aceptar") { dialog, which ->
            //startScanning()
        }
        builder.show()
    }

    private fun showGenerateQRDialog() {

        Handler(Looper.getMainLooper()).postDelayed({
            bitmapFrontImage = null
            bitmapBackImage = null
            bitmapFaceImage = null
            bitmapSignatureImage = null
            dataIne = DataIne()
            clearForm(binding.llContainer)
            binding.nsvContainer.scrollTo(0,0)
        }, 500L)

        //binding.nsvContainer.fullScroll(View.FOCUS_UP)
        val generateQRDialog = GenerateQRDialog()
        generateQRDialog.setGenerateQrDialogListener(object : GenerateQRDialog.GenerateQrDialogListener {
            override fun onScanNew() {
                startScanning()
            }

        })
        generateQRDialog.show(requireActivity().supportFragmentManager,"GenerateQRDialog")
    }

    private fun isSafeShowProgress(): Boolean {
        return mWait != null && !mWait.isVisible
    }

    private fun showDialogMessagePermission() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(getString(R.string.label_notice))
        builder.setMessage(getString(R.string.label_storage_permission_required))

        builder.setPositiveButton(getString(R.string.label_ok)) { dialog, which ->
            checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, AppConstants.STORAGE_PERMISSION_CODE)
        }
        builder.setNegativeButton(getString(R.string.label_leave)) { dialog, which ->
            requireActivity().finish()
        }
        builder.show()
    }

}