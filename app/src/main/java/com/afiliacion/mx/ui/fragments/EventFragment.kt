package com.afiliacion.mx.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.afiliacion.mx.MainViewModel
import com.afiliacion.mx.databinding.FragmentEventBinding
import com.afiliacion.mx.model.entities.Event
import com.afiliacion.mx.ui.adapters.EventsAdapter

class EventFragment : Fragment() {

    private var _binding: FragmentEventBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEventBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        val adapter = EventsAdapter(getEvents())
        binding.rvEvents.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvEvents.adapter = adapter
    }

    private fun getEvents(): ArrayList<Event> {
        val items = ArrayList<Event>()
        for (i in 1..10) {
            val item = Event(
                "Nombre del evento nombre del evento $i",
                "Descripcion del evento descripcion del evento Descripcion del evento Descripcion del evento",
                "Ubicación del evento",
                100000L
            )
            items.add(item)
        }
        return items
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}