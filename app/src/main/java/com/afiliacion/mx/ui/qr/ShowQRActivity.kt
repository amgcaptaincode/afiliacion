package com.afiliacion.mx.ui.qr

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.afiliacion.mx.R
import com.afiliacion.mx.databinding.ActivityShowQrBinding
import com.afiliacion.mx.utils.Toolbox
import com.bumptech.glide.Glide


class ShowQRActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShowQrBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityShowQrBinding.inflate(layoutInflater)

        setContentView(binding.root)
        binding.btnShareQrCode.isEnabled = false

        binding.btnGenerateQr.setOnClickListener {
            generateQr()
        }

        binding.btnShareQrCode.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_STREAM, uriToImage)
            shareIntent.type = "image/*"
            startActivity(
                Intent.createChooser(
                    shareIntent,
                    resources.getText(R.string.label_share_file)
                )
            )

        }

    }

    private fun generateQr() {
        val bitmapQR = Toolbox.getQRCode(baseContext, "Aldair")
        if (bitmapQR != null) {
            binding.btnShareQrCode.isEnabled = true
            Glide.with(baseContext)
                .load(bitmapQR)
                .into(binding.ivShowQr)
            generateImageFile(bitmapQR)
        }
    }

    private lateinit var uriToImage: Uri

    private fun generateImageFile(bitmap: Bitmap) {

        val file = Toolbox.saveImageToExternalStorage(baseContext, bitmap, "abcd")

        if (file != null) {
            uriToImage = file
            Glide.with(baseContext)
                .load(file.toString())
                .centerCrop()
                .into(binding.ivShowQrAsImage)
        }

    }


}