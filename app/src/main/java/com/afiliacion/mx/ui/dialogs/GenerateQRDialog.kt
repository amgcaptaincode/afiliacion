package com.afiliacion.mx.ui.dialogs

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.afiliacion.mx.MainViewModel
import com.afiliacion.mx.R
import com.afiliacion.mx.databinding.DialogGenerateQrBinding
import com.afiliacion.mx.model.entities.DataIne
import com.afiliacion.mx.utils.Toolbox
import com.bumptech.glide.Glide

class GenerateQRDialog : DialogFragment() {

    private var _binding: DialogGenerateQrBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var uriToImage: Uri

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogGenerateQrBinding.inflate(inflater, container, false)
        isCancelable = false
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        viewModel.getDataIne()?.let {
            binding.tvName.text = it.nombre
            binding.tvElectorKey.text = it.claveElector
            generateQr(it)
        }
    }

    private fun initListeners() {
        binding.btnShareQrCode.setOnClickListener {
            shareQRCoe()
        }

        binding.btnScanNew.setOnClickListener {
            if (this::mListener.isInitialized && mListener != null) {
                dismiss()
                mListener.onScanNew()
            }
        }
    }

    override fun getTheme(): Int {
        return R.style.DialogTheme_Loading
    }

    private fun generateQr(dataIne: DataIne) {
        val bitmapQR = Toolbox.getQRCode(requireContext(), dataIne.claveElector!!)
        if (bitmapQR != null) {
            binding.btnShareQrCode.isEnabled = true
            generateImageFile(bitmapQR, dataIne.nombre!!)
        }
    }

    private fun generateImageFile(bitmap: Bitmap, name: String) {

        val file = Toolbox.saveImageToExternalStorage(requireContext(), bitmap, name)
        //val file = Toolbox.storeImage(bitmap, name)

        //if (!file.isEmpty()) {
        if (file != null) {
            // TODO Revisar que funciona en version inferiores a android Q
            //uriToImage = Uri.parse(file)
            uriToImage = file
            Glide.with(requireContext())
                .load(file.toString())
                .centerCrop()
                .into(binding.ivShowQrAsImage)
        }
    }

    private fun shareQRCoe() {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_STREAM, uriToImage)
        shareIntent.type = "image/jpeg"
        startActivity(
            Intent.createChooser(
                shareIntent,
                resources.getText(R.string.label_share_file)
            )
        )
    }



    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private lateinit var mListener: GenerateQrDialogListener

    fun setGenerateQrDialogListener(listener: GenerateQrDialogListener) {
        this.mListener = listener
    }

    interface GenerateQrDialogListener {

        fun onScanNew();

    }

}