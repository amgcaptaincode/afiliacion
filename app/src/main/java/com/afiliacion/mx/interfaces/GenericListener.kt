package com.afiliacion.mx.interfaces

interface GenericListener {

    fun showLoading()

    fun hideLoading()

    fun showWarningDialog(message: String)

}